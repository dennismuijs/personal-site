# README #

My personal site as shown on www.dennismuys.nl

### How do I get set up? ###

* Check out the master branch
* Make sure you can run **npm** commands on your system
* Run **npm install** from the projects root directory
* Use **http-server** or a similar program to host the site