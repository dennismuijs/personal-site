/**
 * Created by dennis on 16/11/2016.
 */

"use strict";

var quoteModule = angular.module("quoteModule", []);

quoteModule.service('quote',[function(){
    var service = {
        quotes: [
            {txt:"Any product that needs a manual to work is broken.", src:"Elon Musk"},
            {txt:"I think it's very important to have a feedback loop, where you're constantly thinking about what you've done and how you could be doing it better.", src:"Elon Musk"},
            {txt:"There is beauty when something works and it works intuitively.", src:"Jonathan Ive"},
            {txt:"Design is a word that's come to mean so much that it's also a word that has come to mean nothing.", src:"Jonathan Ive"}
        ],
        getQuote: function () {
            return service.quotes[Math.floor(Math.random() * service.quotes.length)];
        }
    };
    return service;
}]);