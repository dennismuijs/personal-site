/**
 * Created by dennis on 16/11/2016.
 */

"use strict";

var portfolio = angular.module("portfolio", ['ngAnimate', 'ui.bootstrap', 'ngRoute', 'pascalprecht.translate','mobCheckModule','quoteModule']);

portfolio.config(['$translateProvider','$compileProvider',function($translateProvider,$compileProvider){

    $translateProvider.useStaticFilesLoader({
        prefix: 'assets/i18n/locale-',
        suffix: '.json'
    });
    $translateProvider.uniformLanguageTag('bcp47');
    $translateProvider.preferredLanguage('nl');
    $translateProvider.fallbackLanguage('en');
    $translateProvider.useSanitizeValueStrategy(null);

    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|http?|file|maps|geo|tel|mailto|sms|chrome-extension):/);

}]);

portfolio.controller('MainCtrl',['$scope','quote','mobCheck','$translate', function($scope, quote, mobCheck, $translate){

    $scope.isMobile = mobCheck.isMobile();
    $scope.isCollapsed = true;
    $scope.quote = quote.getQuote();
    $scope.nlActive = true;

    $scope.onLanguageChange = function(lang){
        $translate.use(lang);
        $scope.nlActive = !$scope.nlActive
    };

}]);