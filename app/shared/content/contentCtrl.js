portfolio.controller('contentCtrl',['$scope','$uibModal', function($scope,$uibModal){

    $scope.contentShown = 1;

    $scope.contentSrcs = {
        'twod': [
            "assets/img/2d-01.png",
            "assets/img/2d-02.png",
            "assets/img/2d-03.png",
            "assets/img/2d-04.png",
            "assets/img/2d-05.png"
        ],
        'mobile': [
            "assets/img/mobile-01.png",
            "assets/img/mobile-02.png",
            "assets/img/mobile-03.png",
            "assets/img/mobile-04.png",
            "assets/img/mobile-05.png"
        ],
        'threed': [
            "assets/img/3d-01.png",
            "assets/img/3d-02.png",
            "assets/img/3d-03.png",
            "assets/img/3d-04.png",
            "assets/img/3d-05.png"
        ]
    }

    $scope.open = function (size, src) {
        var $scope = {
            "src": src
        }
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            controllerAs: $scope,
            size: size,
            resolve: {
                src: function () {
                    return $scope.src;
                }
            }
        });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
    });
  };

}]);

portfolio.controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, src) {
  $scope.src = src;
  console.log($scope);
  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});